package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Post")
public class Post {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "title")
	private String title;
	@Column(name = "content")
	private String content; 
	@Column(name = "created_at")
	private Date created_at; 
	@Column(name = "author")
	private String author;
	@Column(name = "longest_read_time")
	private Float longest_read_time;
	@Column(name = "shortest_read_time")
	private Float shortest_read_time;
	@Column(name = "avg_read_time")
	private Float avg_read_time;
	@Column(name = "avg_read_progress")
	private Float avg_read_progress;
	@Column(name = "read_count")
	private Integer read_count;
	@Column(name = "total_read_time")
	private Float total_read_time;
		
	public Post() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Post(int id, String title, String content, Date created_at, String author, Float longest_read_time, Float shortest_read_time, Float avg_read_time, Float avg_read_progress, Integer read_count, Float total_read_time) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.created_at = created_at;
		this.author = author;
		this.longest_read_time = longest_read_time;
		this.shortest_read_time = shortest_read_time;
		this.avg_read_time = avg_read_time;
		this.avg_read_progress = avg_read_progress;
		this.read_count = read_count;
		this.total_read_time = total_read_time;
	}

	public Post(int id, String title, String content, Date created_at, String author, float longest_read_time,
				float shortest_read_time, float avg_read_time, float avg_read_progress, Integer read_count,
				float total_read_time) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.created_at = created_at;
		this.author = author;
		this.longest_read_time = longest_read_time;
		this.shortest_read_time = shortest_read_time;
		this.avg_read_time = avg_read_time;
		this.avg_read_progress = avg_read_progress;
		this.read_count = read_count;
		this.total_read_time = total_read_time;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public float getLongest_read_time() {
		return longest_read_time;
	}

	public void setLongest_read_time(float longest_read_time) {
		this.longest_read_time = longest_read_time;
	}

	public float getShortest_read_time() {
		return shortest_read_time;
	}

	public void setShortest_read_time(float shortest_read_time) {
		this.shortest_read_time = shortest_read_time;
	}

	public float getAvg_read_time() {
		return avg_read_time;
	}

	public void setAvg_read_time(float avg_read_time) {
		this.avg_read_time = avg_read_time;
	}

	public float getAvg_read_progress() {
		return avg_read_progress;
	}

	public void setAvg_read_progress(float avg_read_progress) {
		this.avg_read_progress = avg_read_progress;
	}

	public int getRead_count() {
		return read_count;
	}

	public void setRead_count(Integer read_count) {
		this.read_count = read_count;
	}

	public float getTotal_read_time() {
		return total_read_time;
	}

	public void setTotal_read_time(float total_read_time) {
		this.total_read_time = total_read_time;
	}

	public void setLongest_read_time(Float longest_read_time) {
		this.longest_read_time = longest_read_time;
	}

	public void setShortest_read_time(Float shortest_read_time) {
		this.shortest_read_time = shortest_read_time;
	}

	public void setAvg_read_time(Float avg_read_time) {
		this.avg_read_time = avg_read_time;
	}

	public void setAvg_read_progress(Float avg_read_progress) {
		this.avg_read_progress = avg_read_progress;
	}

	public void setTotal_read_time(Float total_read_time) {
		this.total_read_time = total_read_time;
	}
}
