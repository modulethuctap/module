package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PageSetting")
public class PageSetting {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id; 
	@Column(name = "created_at")
	private Date created_at; 
	@Column(name = "created_by")
	private String created_by; 
	@Column(name = "read_time_out")
	private float read_time_out;
	@Column(name = "read_time_in")
	private float read_time_in;
	
	public PageSetting() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PageSetting(int id, Date created_at, String created_by, float read_time_out, float read_time_in) {
		super();
		this.id = id;
		this.created_at = created_at;
		this.created_by = created_by;
		this.read_time_out = read_time_out;
		this.read_time_in = read_time_in;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public float getRead_time_out() {
		return read_time_out;
	}

	public void setRead_time_out(float read_time_out) {
		this.read_time_out = read_time_out;
	}

	public float getRead_time_in() {
		return read_time_in;
	}

	public void setRead_time_in(float read_time_in) {
		this.read_time_in = read_time_in;
	} 
	
}
