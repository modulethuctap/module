package implement;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import dao.HibernateUtil;
import dao.IPostDAO;
import entity.Post;

public class PostDAOImp implements IPostDAO<Post, Integer>{
	Session session;
	
	@Override
	public List<Post> getAll() {
		System.out.println("try get all");
		session = HibernateUtil.getSessionFactory().openSession();

		List<Post> ls = new ArrayList<Post>();
		try {
			ls = session.createQuery("from Post").list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		session.close();
		return ls;
	}

	@Override
	public Post getById(Integer id) {
		// TODO Auto-generated method stub
		session = HibernateUtil.getSessionFactory().openSession();
		Post post = null;
		try {
			Query query = session.createQuery("from Post where id = :id").setParameter("id",id);
			post = (Post) query.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		session.close();
		return post;
	}

	@Override
	public Integer insert(Post e) {
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Integer toReturn = (Integer) session.save(e);
		session.getTransaction().commit();
		session.close();
		return toReturn;
	}

	@Override
	public Integer update(Post e) {
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.update(e);
		session.getTransaction().commit();
		session.close();
		return 1;
	}

	@Override
	public boolean delete(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

	

}
