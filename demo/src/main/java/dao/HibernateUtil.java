package dao;

import entity.Post;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtil {
	private static SessionFactory sessionFactory;
//	static {
//		if (sessionFactory == null) {
//			StandardServiceRegistry reg = new StandardServiceRegistryBuilder().configure().build();
//			MetadataSources sources = new MetadataSources(reg);
//			Metadata metadata = sources.getMetadataBuilder().build();
//			sessionFactory = metadata.getSessionFactoryBuilder().build();
//		}
//	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null){
			try {
				Configuration conf = new Configuration();
				Properties settings = new Properties();
				settings.put(Environment.DRIVER,"com.microsoft.sqlserver.jdbc.SQLServerDriver");
				settings.put(Environment.URL,"jdbc:sqlserver://localhost:1433;DatabaseName=ModulePost;encrypt=true;trustServerCertificate=true;");
				settings.put(Environment.USER,"sa");
				settings.put(Environment.PASS,"1234$");
				settings.put(Environment.DIALECT,"org.hibernate.dialect.SQLServer2008Dialect");
				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS,"thread");
				settings.put(Environment.SHOW_SQL,"true");

				conf.setProperties(settings);
				conf.addAnnotatedClass(Post.class);

				ServiceRegistry regis = new StandardServiceRegistryBuilder().applySettings(conf.getProperties()).build();
				sessionFactory = conf.buildSessionFactory(regis);
			} catch (Exception e){
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}
}
