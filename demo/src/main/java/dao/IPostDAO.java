package dao;

import java.util.List;

public interface IPostDAO<T,K> {
	public List<T> getAll();
	public T getById(K id);
	public K insert(T e);
	public K update(T e);
	public boolean delete(K id);
}
