package services;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

import entity.Post;
import implement.PostDAOImp;

@Path("/blog")
public class PostService {
	PostDAOImp db;
	Gson gson;
	public PostService() {
		db = new PostDAOImp();
		gson = new Gson();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	
	public String allPost() {
		return gson.toJson(db.getAll());
	}
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/setUserTime")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public String setUserTime() {
		return "setUserTime";
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public String getPost(@PathParam("id") Integer id) {
		return gson.toJson(db.getById(id));
	}

	@Produces(MediaType.APPLICATION_JSON)
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public String addPost(String data) {
		Gson gson = new Gson();
		Post post = gson.fromJson(data, Post.class);
		Integer ok = db.insert(post);
		if (ok != null) {
			return "{msg:'Insert Successfully'}";
		} else {
			return "{msg:'Insert Failed'}";
		}   
	}
	
	@Produces(MediaType.APPLICATION_JSON)
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public String editPost() {
		return "editPost";
	}
	
	@Produces(MediaType.APPLICATION_JSON)
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public String deletePost() {
		return "deletePost";
	}
}
