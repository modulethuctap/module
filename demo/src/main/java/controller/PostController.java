package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/blog")
public class PostController {
    @RequestMapping("")
    public String getAllPost(){
        return "skeleton/postHomeSkeleton";
    }
}
