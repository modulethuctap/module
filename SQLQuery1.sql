CREATE DATABASE ModulePost;

USE ModulePost;

CREATE TABLE Post(
	id int PRIMARY KEY IDENTITY,
	title nvarchar(100) not null,
	content ntext,
	created_at DATETIME,
	author nvarchar(100),
	longest_read_time float,
	shortest_read_time float,
	avg_read_time float,
	avg_read_progress float,
	read_count int,
	total_read_time float
)

CREATE TABLE PageSetting(
	id int PRIMARY KEY IDENTITY,
	created_at DATETIME,
	created_by nvarchar(100),
	read_time_out float,
	read_time_in float
)


INSERT INTO Post VALUES (N'Epic',N'asdasdasdasdasdasdasdasdasdasdaSDasdasdasdasdasdasdasdsad','11-11-2002',N'Hung',null,null,null,null,null)